program benchmark_2d_grid

    use mpi_f08
    use precisions
    use collectives

    implicit none

    integer :: arg_count      ! counter

    ! MPI
    integer(kind=i4)  :: root, my_id, num_proc
    TYPE(MPI_Comm)    :: mpi_comm_cols, mpi_comm_rows, blacs_ctxt
    character(len=MPI_MAX_ERROR_STRING) :: mpierr_string   ! error handling
    integer(kind=i4)  :: mpierr, err_str_len, mpi_err_hdl
    logical  :: dead

    ! BLACS
    integer(kind=i8)  :: np_rows_check, np_cols_check   ! BLACS provided by intel MKL requires int8 
    integer(kind=i8)  :: np_cols, np_rows, my_pcol, my_prow 
    character(len=1)  :: layout
    character(len=2)  :: input_nprows


    call mpi_init(mpierr)
    call mpi_comm_rank(MPI_COMM_WORLD, my_id, mpierr)
    call mpi_comm_size(MPI_COMM_WORLD, num_proc, mpierr)
    root = 0   ! set rank 0 to root

    if (my_id == root) then
        arg_count = command_argument_count()
        if (arg_count > 2)  print *,"Warning: more than two input arguments. Extra arguments will be ignored."
        if (arg_count < 2) then
          print *, "Error: too few input arguments. Please specify the grid layout as well as the no. processor rows."
          stop 1
        else ! i.e. arg_count = 2
          call get_command_argument(1, layout)       ! R | C
          call get_command_argument(2, input_nprows) ! >= 0, if == 0, np_rows will be auto-calculated
          print *, "Specified layout: ", layout
          read (input_nprows, *) np_rows
        end if
    end if
    call mpi_bcast(layout, 1, MPI_CHARACTER, 0, MPI_COMM_WORLD, mpierr)
    call mpi_bcast(np_rows, 1, MPI_INTEGER8, 0, MPI_COMM_WORLD, mpierr)   ! Rem.: with intel MKL, BLACS param. must be int8

    ! setup BLACS grid      
    blacs_ctxt = MPI_COMM_WORLD

    do np_cols = NINT(SQRT(REAL(num_proc))),2,-1
      if(mod(num_proc, int(np_cols)) == 0 ) exit
    enddo

    if (np_rows <= 0)  np_rows = num_proc/np_cols
    if (my_id == root)  then
      print *, "number of processes: ", num_proc
      print *, "np_rows: ", np_rows
      print *, "np_cols: ", np_cols
    end if

    !Initialize:
    np_rows_check = 0_i8;   np_cols_check = 0_i8
    call BLACS_Gridinit(blacs_ctxt, layout, np_rows, np_cols)
    call BLACS_Gridinfo(blacs_ctxt, np_rows_check, np_cols_check, my_prow, my_pcol)

    if (my_prow > 0) then   ! if BLACS uses less ranks than avail., extra ranks get a negative ID
      if (np_rows /= np_rows_check) then
        print *,"BLACS_Gridinfo returned different np_row values: ",np_rows_check 
        stop 1
      endif
    end if

    if (my_pcol > 0) then
      if (np_cols /= np_cols_check) then
        print *,"BLACS_Gridinfo returned different np_col values",np_cols_check
        stop 1
      endif
    end if

    ! Split communicators (row and col)
    call mpi_comm_split(MPI_COMM_WORLD, int(my_pcol), int(my_prow), &
                        mpi_comm_rows, mpierr)
    if (mpierr .ne. MPI_SUCCESS) then
      call MPI_ERROR_STRING(mpierr, mpierr_string, err_str_len, mpi_err_hdl)
      print *, "Error in mpi_comm_split for mpi_comm_rows: ", trim(mpierr_string)
      stop 1
    endif

    call mpi_comm_split(MPI_COMM_WORLD, int(my_prow), int(my_pcol), &
                        mpi_comm_cols, mpierr)
    if (mpierr .ne. MPI_SUCCESS) then
      call MPI_ERROR_STRING(mpierr, mpierr_string, err_str_len, mpi_err_hdl)
      print *, "Error in mpi_comm_split for mpi_comm_cols: ", trim(mpierr_string)
      stop 1
    endif
    
    call benchmark(mpi_comm_rows, mpi_comm_cols, np_rows, np_cols, my_id, num_proc, layout)
    
    ! end of benchmark
    if (my_id == root) then
        print *, "Benchmark finished successfully."
    endif

    call blacs_exit(1)
    call mpi_finalized(dead, mpierr)

    if(.not. dead) call mpi_finalize(mpierr)

end program benchmark_2d_grid

module utils

  use mpi_f08
  use precisions

  implicit none

contains
  subroutine get_layout(layout)
    ! handles input specifying the BLACS grid layout.
    ! valid inputs are: R | C

    character(len=1), intent(out)   :: layout

    call get_command_argument(1, layout)    
    if (layout /= 'R' .and. layout /= 'C') then
        print *, "Invalid layout specified. Choose between R and C. Aborting execution."
        stop 'ERROR' 
    else
        print *, "Specified layout: ", layout
    end if

  end subroutine get_layout

  subroutine setup_blacs(blacs_ctxt, layout, my_prow, my_pcol, np_rows, np_cols, num_proc)
 
    TYPE(MPI_Comm), intent(out)     :: blacs_ctxt
    character(len=1), intent(in)   :: layout
    integer, intent(in)   ::  num_proc
    integer, intent(out)  ::  my_prow, my_pcol, np_rows, np_cols
    integer               ::  np_rows_check, np_cols_check


    do np_cols = NINT(SQRT(REAL(num_proc))),2,-1
      if(mod(num_proc, np_cols) == 0 ) exit
    enddo

    np_rows = num_proc/np_cols

    blacs_ctxt = MPI_COMM_WORLD
    call BLACS_Gridinit(blacs_ctxt, layout, np_rows, np_cols)

    call BLACS_Gridinfo(blacs_ctxt, np_rows_check, & 
                        np_cols_check, my_prow, my_pcol)

    if (np_rows /= np_rows_check) then
      print *,"BLACS_Gridinfo returned different np_row values"
      stop 'ERROR'
    endif

    if (np_cols /= np_cols_check) then
      print *,"BLACS_Gridinfo returned different np_col values"
      stop 'ERROR'
    endif

  end subroutine setup_blacs


  subroutine setup_mpi_comms(my_pcol, my_prow, mpi_comm_rows, mpi_comm_cols)

    integer, intent(in)           ::   my_pcol, my_prow
    TYPE(MPI_Comm), intent(out)   ::   mpi_comm_rows, mpi_comm_cols
    ! error handling
    integer    :: mpierr, err_str_len, mpi_err_hdl
    character(len=MPI_MAX_ERROR_STRING) :: mpierr_string

    call mpi_comm_split(MPI_COMM_WORLD, my_pcol, my_prow, mpi_comm_rows, mpierr)

    if (mpierr .ne. MPI_SUCCESS) then
      call MPI_ERROR_STRING(mpierr, mpierr_string, err_str_len, mpi_err_hdl)
      print *, "Error in mpi_comm_split for mpi_comm_rows: ", trim(mpierr_string)
      stop 'ERROR'
    endif

    call mpi_comm_split(MPI_COMM_WORLD, my_prow, my_pcol, mpi_comm_cols, mpierr)
    if (mpierr .ne. MPI_SUCCESS) then
      call MPI_ERROR_STRING(mpierr, mpierr_string, err_str_len, mpi_err_hdl)
      print *, "Error in mpi_comm_split for mpi_comm_cols: ", trim(mpierr_string)
      stop 'ERROR'
    endif
  end subroutine setup_mpi_comms

  subroutine io(field, min_val, max_val, mean, std, num_iterations)
  
    character(len=*), intent(in) :: field
    real(kind=dp), intent(in)    :: min_val, max_val, mean, std
    integer, intent(in)          :: num_iterations

      print *, "Execution time (s) for ", field, ": "
      print *, "Min.: ", min_val, 'per call: ', min_val/num_iterations
      print *, "Max.: ", max_val, 'per call: ', max_val/num_iterations
      print *, "Avg.: ", mean, 'per call: ', mean/num_iterations
      print *, "Std.: ", std, 'per call: ', std/num_iterations
      print *, new_line('A')

  end subroutine io

end module utils

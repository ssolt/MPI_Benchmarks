module precisions

  use iso_fortran_env

  implicit none
  
  integer, parameter   ::   dp=real64
  integer, parameter   ::   i4=int32
  integer, parameter   ::   i8=int64
end module precisions

module stats

  use precisions

  implicit none


contains

  real(kind=dp) function average(input_array)

    real(kind=dp), intent(in)   :: input_array(:)
    integer    ::    data_count

    data_count = size(input_array, 1)
    if (data_count /= 0) then
      average = sum(input_array, 1) / data_count
    else
      print *, "Error: empty array was passed."
      stop 'ERROR'
    end if
  end function average

  real(kind=dp) function std(input_array, avg)

    real(kind=dp), intent(in)  ::  input_array(:)
    real(kind=dp), intent(in)  ::  avg
    integer          ::  data_count, i
    real(kind=dp)    ::  summation

    summation = 0.0_dp
    data_count = size(input_array, 1)
    do i=1, data_count
     summation = summation + (input_array(i) - avg)**2
    end do

    std = sqrt(summation/data_count)
  end function std

  subroutine extract_stats(input_array, min_val, max_val, avg, std_dev)

    real(kind=dp), intent(in)   :: input_array(:)
    real(kind=dp), intent(out)  :: min_val, max_val, avg, std_dev

    min_val = minval(input_array)
    max_val = maxval(input_array)
    avg = average(input_array)
    std_dev = std(input_array, avg)

  end subroutine extract_stats

end module stats

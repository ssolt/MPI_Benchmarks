# MPI_Benchmarks
MPI benchmarks for performance analysis of collective operations


# Build
Currently, only Intel toolchain is supported for using BLACS. The underlying MPI library can, however, be chosen to be either Intel MPI or OpenMPI. 

## Dependencies
- CMake v3.18 or above
- Intel MKL (needed for the BLACS)
- working Fortran compiler providing the mpi_f08 module
- MPI library

## Using BLACS to set up a 2D processor grid
First, the `MKLROOT` environment variable should be set to point to where Intel MKL is installed on the target system:

```export MKLROOT=/path/to/MKL```

The next steps depend on the toolchain being used. For the **Intel** toolchain, the CMake Fortran compiler should be set to `mpiifort`:

`export FC="mpiifort"`

Then, while in the package main directory, `cmake -B build` configures the build and creates the build directory.

For using BLACS provided by Intel MKL together with **OpenMPI**, the CMake Fortran compiler should be set to `mpif90`:

`export FC="mpif90"`

Next, the build system should be set up by specifying the variable `WITH_OPENMPI=yes`: 

`cmake -B build  -DWITH_OPENMPI=yes`

Finally:

```cmake --build build```

creates the executable called `collectives_blacs` inside the build directory.

## Launching the benchmark
The executable should be launched by the MPI launcher. Two input arguments must be passed: i) the processor layout, and ii) the number of processor rows:

`mpi_launcher collectives_blacs <layout> <np_rows>`

The layout can be either C or R corresponding to a column-major or a row-major ordering, respectively. If it is desired to set the number of processor rows, a positive value should be passed as the second argument. To let the benchmark decide on the number of processor rows, the second argument should be 0 or negative.

Upon a successful run, the results will be printed to the standard output. In principle, depending on the grid layout, communication may be slower across processor rows or columns. However, a too large difference between the performance of collectives over the two paths might indicate the presence of an underlying issue.

